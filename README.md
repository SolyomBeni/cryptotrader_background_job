# obudai-functions

# running in local

## azure function tools

```
npm i -g azure-functions-core-tools@core
```

## AzureWebJobsStorage

Update the `AzureWebJobsStorage` in `local.settings.json` with the value generated in the Azure portal for the Function App

## run the function

```
cd Tools
./run-functions.sh
```