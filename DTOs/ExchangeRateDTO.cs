﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObudaiFunctions.DTOs
{
    class ExchangeRateDTO
    {
        public double CurrentAmount { get; set; }
        public double Multiplier { get; set; }
        public string Symbol { get; set; }
        public Double CurrentRate { get; set; }
        public string LastRefreshed { get; set; }
        public string TimeZone { get; set; }
        public Dictionary<string, double> History { get; set; }
    }
}
